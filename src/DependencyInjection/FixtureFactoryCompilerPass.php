<?php
declare(strict_types=1);

namespace CodeMySoft\Fixtures\DependencyInjection;

use CodeMySoft\Fixtures\Infrastructure\Fixtures\Factory\FixtureFactoryProvider;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class FixtureFactoryCompilerPass implements CompilerPassInterface
{
    public function process(
        ContainerBuilder $container
    ) {
        $fixtureFactoryDefinition = $container->findDefinition(FixtureFactoryProvider::class);

        $fixtureFactoryIds = $container->findTaggedServiceIds('codemysoft.fixture.factories');

        foreach ($fixtureFactoryIds as $factoryId => $tags) {
            $fixtureFactoryDefinition->addMethodCall('addFactory', [new Reference($factoryId)]);
        }
    }
}
