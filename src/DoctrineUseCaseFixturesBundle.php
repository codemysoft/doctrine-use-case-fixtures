<?php
declare(strict_types=1);

namespace CodeMySoft\Fixtures;

use CodeMySoft\Fixtures\DependencyInjection\FixtureFactoryCompilerPass;
use CodeMySoft\Fixtures\Infrastructure\Fixtures\Factory\AbstractFixtureFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DoctrineUseCaseFixturesBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->registerForAutoconfiguration(AbstractFixtureFactory::class)
            ->addTag('codemysoft.fixture.factories');

        $container->addCompilerPass(new FixtureFactoryCompilerPass());
    }
}