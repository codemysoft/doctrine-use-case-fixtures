<?php
declare(strict_types=1);

namespace CodeMySoft\Fixtures\Infrastructure\Fixtures\Factory;

use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Persistence\ObjectManager;
use CodeMySoft\Fixtures\Infrastructure\Fixtures\FixtureLoader;
use Symfony\Component\Uid\Uuid;

abstract class AbstractFixtureFactory
{
    protected FixtureLoader $loader;

    public function create(
        FixtureLoader $fixtureLoader,
        ObjectManager $objectManager,
        ReferenceRepository $referenceRepository,
        $data
    ) {
        if (is_object($data)) {
            return $data;
        }

        if (!is_array($data)) {
            throw new \InvalidArgumentException('Invalid type.');
        }

        $this->loader = $fixtureLoader;
        $id = $this->prepareId($data);

        if ($referenceRepository->hasReference($id->toRfc4122())) {
            return $referenceRepository->getReference($id->toRfc4122());
        }


        $object = $this->createObject($id, $data);

        $objectManager->persist($object);
        $referenceRepository->addReference($id->toRfc4122(), $object);

        return $object;
    }

    abstract public function className(): string;

    abstract protected function createObject(Uuid $id, array $data = []): object;

    protected function load(string $className, $data): object
    {
        return $this->loader->load($className, $data);
    }

    private function prepareId($data): Uuid
    {
        if (!isset($data['id'])) {
            return Uuid::v1();
        }

        if ($data['id'] instanceof Uuid) {
            return $data['id'];
        }

        return Uuid::fromString($data['id']);
    }
}
