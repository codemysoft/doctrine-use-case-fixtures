<?php
declare(strict_types=1);

namespace CodeMySoft\Fixtures\Infrastructure\Fixtures\Factory;

class FixtureFactoryProvider
{
    /**
     * @var AbstractFixtureFactory[]
     */
    private array $factories = [];

    /**
     * In standard, factories will be filled by compiler pass
     *
     * @see FixtureFactoryCompilerPass
     */
    public function addFactory(AbstractFixtureFactory $factory): void
    {
        $this->factories[$factory->className()] = $factory;
    }

    public function getFactory(string $className): AbstractFixtureFactory
    {
        if (isset($this->factories[$className])) {
            return $this->factories[$className];
        }

        throw new \InvalidArgumentException('FixtureFactory not implemented for ' . $className);
    }
}
