<?php
declare(strict_types=1);

namespace CodeMySoft\Fixtures\Infrastructure\Fixtures;

use Doctrine\Common\DataFixtures\ReferenceRepository;
use CodeMySoft\Fixtures\Infrastructure\Fixtures\Factory\FixtureFactoryProvider;
use Doctrine\Persistence\ObjectManager;

class FixtureLoader
{
    private ObjectManager $objectManager;
    private ReferenceRepository $referenceRepository;
    private FixtureFactoryProvider $factoryProvider;

    public function __construct(FixtureFactoryProvider $factoryProvider)
    {
        $this->factoryProvider = $factoryProvider;
    }

    public function setObjectManager(ObjectManager $objectManager): void
    {
        $this->objectManager = $objectManager;
    }

    public function setReferenceRepository(ReferenceRepository $referenceRepository): void
    {
        $this->referenceRepository = $referenceRepository;
    }

    public function load(string $className, $data = []): object
    {
        $factory = $this->factoryProvider->getFactory($className);

        return $factory->create($this, $this->objectManager, $this->referenceRepository, $data);
    }

    /**
     * @return object[]
     */
    public function loadMany(string $className, int $count, array $data = []): array
    {
        $objects = [];

        for ($i = 0; $i < $count; $i++) {
            $objects[] = $this->load($className, $data);
        }

        return $objects;
    }
}
