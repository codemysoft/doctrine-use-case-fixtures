<?php

declare(strict_types=1);

namespace CodeMySoft\Fixtures\Infrastructure\Fixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

abstract class UseCaseFixture extends Fixture implements FixtureGroupInterface
{
    private FixtureLoader $fixtureLoader;

    public function __construct(FixtureLoader $fixtureLoader)
    {
        $this->fixtureLoader = $fixtureLoader;
    }

    public function load(ObjectManager $manager): void
    {
        $this->preLoad();

        $this->fixtureLoader->setReferenceRepository($this->referenceRepository);
        $this->fixtureLoader->setObjectManager($manager);

        $this->doLoad($this->fixtureLoader);

        $manager->flush();

        $this->postLoad();
    }

    /**
     * Does nothing unless you overwrite it
     */
    protected function preLoad(): void
    {
    }

    /**
     * Does nothing unless you overwrite it
     */
    protected function postLoad(): void
    {
    }

    abstract protected function doLoad(FixtureLoader $loader): void;

    public static function getGroups(): array
    {
        return [static::class];
    }
}
